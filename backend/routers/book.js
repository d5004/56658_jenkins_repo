const { request, response } = require('express')
const express = require('express')
const router = express.Router()
const db = require('../db')
const utils = require('../utils')


//api for adding book.
router.post('/',(request,response)=>{
    const{title,publisher_name,author_name} = request.body
    const query = `insert into Book values(default,'${title}','${publisher_name}','${author_name}')`

    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//api for displaying book.
router.get('/all',(request,response)=>{
    const query = `select * from Book`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//api for updating data
router.put('/:book_id',(request,response)=>{
    const{book_id} = request.params
    const{ publisher_name ,Author_name} = request.body
    const query =`update Book set publisher_name='${publisher_name}',author_name='${Author_name}' where book_id='${book_id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//api for deleting book
router.delete('/:book_id',(request,response)=>{
    const{book_id} = request.params
    const query =`delete from Book where book_id='${book_id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router