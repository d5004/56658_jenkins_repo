const express = require('express')
const cors = require('cors')
const app = express()
const bookRouter = require('./routers/book')

app.use(cors('*'))
app.use(express.json())
app.use('/book', bookRouter)

app.listen(4000, '0.0.0.0',()=>{
    console.log('server started on port 4000')
})