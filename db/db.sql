create table Book(
    book_id int primary key auto_increment,
    book_title varchar(50), 
    publisher_name varchar(50),
    author_name varchar(30)
);